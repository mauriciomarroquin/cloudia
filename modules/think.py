import json
from ibm_watson import AssistantV1
from ibm_cloud_sdk_core.authenticators import IAMAuthenticator
from threading import Thread
from queue import Queue
from utils import Event

authenticator = IAMAuthenticator('')
assistant = AssistantV1(
    version='2018-07-10',
    authenticator=authenticator)
assistant.set_service_url('https://gateway.watsonplatform.net/assistant/api')

workspace_id='38c75554-8482-4648-be72-e44ff58d5767'

class BotThink(Thread):
    
    MODULE_NAME = 'BOTTHINK'
    
    eventqueue = Queue(100)

    
    def evaluate(self, msg):
            print('procesando')
            response = assistant.message(
            workspace_id=workspace_id,
            input={
                'text': msg
            },
            context={
                'metadata': {
                    'deployment': 'myDeployment'
                }
            }).get_result()
            print(json.dumps(response, indent=2))
            if len(response['output']['text'])>0:
                text = response['output']['text'][0];
                self.sendEvent('TALK', text)
            else:
                return''

    def run(self):
                
            self.eventhandler.notify(Event('INFO',self.MODULE_NAME+' READY'))

    
            while True:

               if not self.eventqueue.empty():
                    print(self.MODULE_NAME + ' message received...')
                    msg = self.eventqueue.get_nowait()
                    print(msg)
                    
                    if(msg[0] == 'INFO'):
                        print('INFO:' + msg[1])
                    elif(msg[0] == 'THINK'):
                        self.evaluate(msg[1])

    def setEventListener(self, botevent):
        self.eventhandler = botevent
        botevent.addSubscriber(self)
        self.eventhandler.notify(Event('INFO', self.MODULE_NAME + ' CONNECTED'))

    def sendEvent(self, type, msg):
        self.eventhandler.notify(Event(type, msg))

    def receiveEvent(self, event):
        self.eventqueue.put_nowait(event)
