# You need to install pyaudio to run this example
# pip install pyaudio

# In this example, the websocket connection is opened with a text
# passed in the request. When the service responds with the synthesized
# audio, the pyaudio would play it in a blocking mode

from ibm_watson.websocket import SynthesizeCallback
import pyaudio
from ibm_cloud_sdk_core.authenticators import IAMAuthenticator
from ibm_watson.text_to_speech_adapter_v1 import TextToSpeechV1Adapter
from queue import Queue
from threading import Thread
from utils import Event

authenticator = IAMAuthenticator('')
service = TextToSpeechV1Adapter(authenticator=authenticator)
service.set_service_url('https://stream.watsonplatform.net/text-to-speech/api')

class BotTalk(Thread):
    
    MODULE_NAME = 'BOTTALK'
    
    eventqueue = Queue(100)
 
    
    talkq = Queue(100)
        
    class Play(object):
            """
            Wrapper to play the audio in a blocking mode
            """
            def __init__(self):
                self.format = pyaudio.paInt16
                self.channels = 1
                self.rate = 22050
                self.chunk = 2048
                self.pyaudio = None
                self.stream = None
        
            def start_streaming(self):
                self.pyaudio = pyaudio.PyAudio()
                self.stream = self._open_stream()
                self._start_stream()
        
       
            def _open_stream(self):
 
                def aucallback(in_data,frame_count,time_info,status):
                 return(in_data,status)
  
                
                stream = self.pyaudio.open(
                    format=self.format,
                    channels=self.channels,
                    rate=self.rate,
                    output=True,
                    frames_per_buffer=self.chunk,
                    start=False#,
                    #stream_callback=aucallback
                )
                return stream
        
            def _start_stream(self):
                self.stream.start_stream()
        
            def write_stream(self, audio_stream):
                print(4)
                self.stream.write(audio_stream)
        
            def complete_playing(self):
                print(5)

                self.stream.stop_stream()
                self.stream.close()
                self.pyaudio.terminate()
        
    class MySynthesizeCallback(SynthesizeCallback):
            def __init__(self, method, parent):
                SynthesizeCallback.__init__(self)
                self.play = method
                self.parent = parent
        
            def on_connected(self):
                print('Opening stream to play')
                self.play.start_streaming()
        
            def on_error(self, error):
                print('Error received: {}'.format(error))
        
            def on_timing_information(self, timing_information):
                print(timing_information)
        
            def on_audio_stream(self, audio_stream):
                self.play.write_stream(audio_stream)
        
            def on_close(self):
                print('Completed synthesizing')
                self.play.complete_playing()
                self.parent.sendEvent('LISTEN', 'RESUME')

            def setParent(self, parent):
                self.parent = parent
        
    def playText(self, text):
            print('Reproduciendo')
            try:
                service.synthesize_using_websocket(text,
                                               self.test_callback,
                                               accept='audio/wav',
                                               voice="en-US_AllisonVoice")
            except:
                print('error')                                  
            print('fin')
    def run(self):
                
            self.test_callback = self.MySynthesizeCallback(self.Play(), self)
                # An example SSML text

            self.eventhandler.notify(Event('INFO',self.MODULE_NAME+' READY'))

            a=1
    
            while True:
               #sprint(a)

               if not self.eventqueue.empty():
                    print(self.MODULE_NAME + ' message received...')
                    msg = self.eventqueue.get_nowait()
                    print(msg)
                    
                    if(msg[0] == 'INFO'):
                        print('INFO:' + msg[1])
                    elif(msg[0] == 'TALK'):
                        self.sendEvent('LISTEN', 'PAUSE')
                        a=2
                        self.playText(msg[1])
                        a=3
                        print(3)
  
                    
    def putText(self, text):
            self.talkq.put_nowait(text)
# bottalk = BotTalk()
# bottalk.putText('Hi');
# bottalk.playText('bye')
    def setEventListener(self, botevent):
        self.eventhandler = botevent
        botevent.addSubscriber(self)
        self.eventhandler.notify(Event('INFO', self.MODULE_NAME + ' CONNECTED'))

    def sendEvent(self, type, msg):
        self.eventhandler.notify(Event(type, msg))

    def receiveEvent(self, event):
        self.eventqueue.put_nowait(event)
