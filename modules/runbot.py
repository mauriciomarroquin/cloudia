from listen import BotListen
from queue import Queue
from think import BotThink
from talk import BotTalk
from event import BotEvent
from utils import Event
from time import sleep

print('Init main system...\n')

q = Queue(100)
#q.put_nowait('hola')

botevent = BotEvent()
botevent.start()

botlisten = BotListen()
botlisten.setEventListener(botevent)
#botlisten.setqueue(q)
#botlisten.start()

bottalk = BotTalk()
bottalk.setEventListener(botevent)
bottalk.start()

botthink = BotThink()
botthink.setEventListener(botevent)
botthink.start()

print('sleeping')
sleep(10)
print('awaken')
botevent.notify(Event('THINK','Tell a joke'))

while True:
    if not q.empty():
        print('Procesando mensajes...')
        #event = q.get_nowait()
        #if event.
        #msg=q.get_nowait()
        #print(msg)
        #response =botthink.evaluate(msg)
        #print(response)
        #botlisten.pause()
        #bottalk.putText(response)
        #botlisten.resume()
        