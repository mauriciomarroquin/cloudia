from threading import Thread
from queue import Queue
from utils import Event


class BotEvent(Thread):
    
    globalqueue = Queue()
    
    subscribers = []
    
    def addSubscriber(self, receiver):
        self.subscribers.append(receiver)
        receiver.receiveEvent(Event('INFO', 'Subscribed to event manager'))
    
    def publish(self, event):
        for i in self.subscribers:
            i.receiveEvent(event)
    
    def notify(self, event):
        self.globalqueue.put(event)
    
    def run(self):
        while True:
            if not self.globalqueue.empty():
                    print('Procesando mensajes...')
                    msg = self.globalqueue.get_nowait()
                    
                    if(msg[0] == 'INFO'):
                        print('INFO:' + msg[1])
                    elif(msg[0] == 'TALK'):
                        print('TALK:' + msg[1])
                        self.publish(msg)
                    elif(msg[0] == 'LISTEN'):
                        print('LISTEN:' + msg[1])
                        self.publish(msg)
                    elif(msg[0] == 'THINK'):
                        print('THINK:' + msg[1])
                        self.publish(msg)
                        
            
