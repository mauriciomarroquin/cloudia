# You need to install pyaudio to run this example
# pip install pyaudio

# When using a microphone, the AudioSource `input` parameter would be
# initialised as a queue. The pyaudio stream would be continuosly adding
# recordings to the queue, and the websocket client would be sending the
# recordings to the speech to text service

import pyaudio
from ibm_watson import SpeechToTextV1
from ibm_watson.websocket import RecognizeCallback, AudioSource
from threading import Thread
from ibm_cloud_sdk_core.authenticators import IAMAuthenticator
from collections import namedtuple
from utils import Event

try:
    from Queue import Queue, Full
except ImportError:
    from queue import Queue, Full

class BotListen(Thread):
    MODULE_NAME = 'BOTLISTEN'
    
    eventqueue = Queue(100)
    
    ###############################################
    #### Initalize queue to store the recordings ##
    ###############################################
    CHUNK = 1024
    # Note: It will discard if the websocket client can't consumme fast enough
    # So, increase the max size as per your choice
    BUF_MAX_SIZE = CHUNK * 10
    # Buffer to store audio
    q = Queue(maxsize=int(round(BUF_MAX_SIZE / CHUNK)))
    
    # Create an instance of AudioSource
    audio_source = AudioSource(q, True, True)
    
    ###############################################
    #### Prepare Speech to Text Service ########
    ###############################################
    
    # initialize speech to text service
    authenticator = IAMAuthenticator('')
    speech_to_text = SpeechToTextV1(authenticator=authenticator)
    
    # define callback for the speech to text service
    class MyRecognizeCallback(RecognizeCallback):
        def __init__(self):
            RecognizeCallback.__init__(self)
    
        def settargetqueue(self,newqueue):
            self.targetqueue=newqueue
        #def on_transcription(self, transcript):
        #   print('transcript:'+transcript)
    
        def on_connected(self):
            print('Connection was successful')
    
        def on_error(self, error):
            print('Error received: {}'.format(error))
    
        def on_inactivity_timeout(self, error):
            print('Inactivity timeout: {}'.format(error))
    
        def on_listening(self):
            print('Service is listening')
    
        def on_hypothesis(self, hypothesis):
            #self.targetqueue.put(hypothesis)
            print('hypo:'+hypothesis)
            self.sendEvent('THINK', hypothesis)

        def setEventListener(self, botevent):
            self.eventhandler=botevent
    
        def sendEvent(self, type, msg):
            self.eventhandler.notify(Event(type,msg))

    
        #def on_data(self, data):
        #   print(data)
    
        def on_close(self):
            print("Connection closed")
    
        
    
    # this function will initiate the recognize service and pass in the AudioSource
    def recognize_using_weboscket(self,*args):
        mycallback = self.MyRecognizeCallback()
        #-smycallback.settargetqueue(self.targetqueue)
        mycallback.setEventListener(self.eventhandler)
        
        self.speech_to_text.recognize_using_websocket(audio=self.audio_source,
                                                 content_type='audio/l16; rate=44100',
                                                 recognize_callback=mycallback,
                                                 interim_results=True)
    
    ###############################################
    #### Prepare the for recording using Pyaudio ##
    ###############################################
    
    # Variables for recording the speech
    FORMAT = pyaudio.paInt16
    CHANNELS = 1
    RATE = 44100
    
    # define callback for pyaudio to store the recording in queue
    def pyaudio_callback(self,in_data, frame_count, time_info, status):
        try:
            self.q.put(in_data)
        except Full:
            pass # discard
        return (None, pyaudio.paContinue)

    def setqueue(self,newqueue):
        self.targetqueue=newqueue
    
    def pause(self):
        self.stream.stop_stream()
        
    def resume(self):
        self.stream.start_stream()    
    
    def run(self):
        # instantiate pyaudio
        audio = pyaudio.PyAudio()
        
        # open stream using callback
        self.stream = audio.open(
            format=self.FORMAT,
            channels=self.CHANNELS,
            rate=self.RATE,
            input=True,
            frames_per_buffer=self.CHUNK,
            stream_callback=self.pyaudio_callback,
            start=False
        )
        
        #########################################################################
        #### Start the recording and start service to recognize the stream ######
        #########################################################################
        
        print("Enter CTRL+C to end recording...")
        self.stream.start_stream()
        
        try:
            recognize_thread = Thread(target=self.recognize_using_weboscket, args=())
            recognize_thread.start()
        
            #self.recognize_using_weboscket()
    
            self.eventhandler.notify(Event('INFO',self.MODULE_NAME+' READY'))

    
            while True:
                if not self.eventqueue.empty():
                    print(self.MODULE_NAME+' message received...')
                    msg=self.eventqueue.get_nowait()
                    print(msg)
                    
                    if(msg[0] == 'INFO'):
                        print('INFO:'+msg[1])
                    elif(msg[0] == 'LISTEN'):
                        if(msg[1] == 'PAUSE'):
                            self.pause()
                        if(msg[1] == 'RESUME'):
                            self.resume()
                        
 
            
            #while True:
             #   pass
        except KeyboardInterrupt:
            # stop recording
            self.stream.stop_stream()
            self.stream.close()
            audio.terminate()
            self.audio_source.completed_recording()

    
    def setEventListener(self, botevent):
        self.eventhandler=botevent
        botevent.addSubscriber(self)
        self.eventhandler.notify(Event('INFO',self.MODULE_NAME+' CONNECTED'))

    def sendEvent(self, type, msg):
        self.eventhandler.notify(Event(type,msg))

    def receiveEvent(self,event):
        self.eventqueue.put_nowait(event)
